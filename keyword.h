#ifndef KEYWORD_H_
#define KEYWORD_H_

#include <limits.h>

#define KW1(C1) ((unsigned long long)(unsigned char)C1)

#define KW2(C1, C2) ( \
	(unsigned long long)(unsigned char)C1 << CHAR_BIT | \
	(unsigned long long)(unsigned char)C2)

#define KW3(C1, C2, C3) ( \
	(unsigned long long)(unsigned char)C1 << CHAR_BIT * 2 | \
	(unsigned long long)(unsigned char)C2 << CHAR_BIT | \
	(unsigned long long)(unsigned char)C3)

#define KW4(C1, C2, C3, C4) ( \
	(unsigned long long)(unsigned char)C1 << CHAR_BIT * 3 | \
	(unsigned long long)(unsigned char)C2 << CHAR_BIT * 2 | \
	(unsigned long long)(unsigned char)C3 << CHAR_BIT | \
	(unsigned long long)(unsigned char)C4)

#define KW5(C1, C2, C3, C4, C5) ( \
	(unsigned long long)(unsigned char)C1 << CHAR_BIT * 4 | \
	(unsigned long long)(unsigned char)C2 << CHAR_BIT * 3 | \
	(unsigned long long)(unsigned char)C3 << CHAR_BIT * 2 | \
	(unsigned long long)(unsigned char)C4 << CHAR_BIT | \
	(unsigned long long)(unsigned char)C5)

#define KW6(C1, C2, C3, C4, C5, C6) ( \
	(unsigned long long)(unsigned char)C1 << CHAR_BIT * 5 | \
	(unsigned long long)(unsigned char)C2 << CHAR_BIT * 4 | \
	(unsigned long long)(unsigned char)C3 << CHAR_BIT * 3 | \
	(unsigned long long)(unsigned char)C4 << CHAR_BIT * 2 | \
	(unsigned long long)(unsigned char)C5 << CHAR_BIT | \
	(unsigned long long)(unsigned char)C6)

#define KW7(C1, C2, C3, C4, C5, C6, C7) ( \
	(unsigned long long)(unsigned char)C1 << CHAR_BIT * 6 | \
	(unsigned long long)(unsigned char)C2 << CHAR_BIT * 5 | \
	(unsigned long long)(unsigned char)C3 << CHAR_BIT * 4 | \
	(unsigned long long)(unsigned char)C4 << CHAR_BIT * 3 | \
	(unsigned long long)(unsigned char)C5 << CHAR_BIT * 2 | \
	(unsigned long long)(unsigned char)C6 << CHAR_BIT | \
	(unsigned long long)(unsigned char)C7)

#define KW8(C1, C2, C3, C4, C5, C6, C7, C8) ( \
	(unsigned long long)(unsigned char)C1 << CHAR_BIT * 7 | \
	(unsigned long long)(unsigned char)C2 << CHAR_BIT * 6 | \
	(unsigned long long)(unsigned char)C3 << CHAR_BIT * 5 | \
	(unsigned long long)(unsigned char)C4 << CHAR_BIT * 4 | \
	(unsigned long long)(unsigned char)C5 << CHAR_BIT * 3 | \
	(unsigned long long)(unsigned char)C6 << CHAR_BIT * 2 | \
	(unsigned long long)(unsigned char)C7 << CHAR_BIT | \
	(unsigned long long)(unsigned char)C8)

inline unsigned long long keyword(const char *string)
{
	const unsigned char *cc = (const unsigned char *)string;
	unsigned long long value = 0;

	for(; *cc; value = (value << CHAR_BIT) | *cc++)
	{
		if((size_t)((const char *)cc - string) >= sizeof value)
			return 0;
	}

	return value;
}

#endif

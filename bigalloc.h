/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#ifndef BIGALLOC_H_
#define BIGALLOC_H_

#include <stddef.h>

#if defined __unix__

#include <sys/mman.h>

static inline void *bigalloc(size_t size)
{
	void *block = mmap(NULL, size, PROT_READ | PROT_WRITE,
		MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);

	return block != MAP_FAILED ? block : NULL;
}

static inline _Bool bigfree(void *block, size_t size)
{
	return munmap(block, size) == 0;
}

#elif defined _WIN32

#include <windows.h>

static inline void *bigalloc(size_t size)
{
	return VirtualAlloc(NULL, size, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
}

static inline _Bool bigfree(void *block, size_t size)
{
	(void)size;
	return VirtualFree(pages, 0, MEM_RELEASE);
}

#else

#include <stdlib.h>

static inline void *bigalloc(size_t size)
{
	return calloc(size, 1);
}

static inline _Bool bigfree(void *block, size_t size)
{
	(void)size;
	free(block);
	return 1;
}

#endif

#endif

/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#ifndef CLOSURE_H_
#define CLOSURE_H_

typedef void (*closure_t)(void);

closure_t closure_create(int *id, void func(void *), const void *arg);
void closure_destroy(int id);

#endif

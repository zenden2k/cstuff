#ifndef W32DLL_H_
#define W32DLL_H_

#include <wchar.h>

extern void *w32dll_load_from_rpath(const wchar_t *rpath);
extern int w32dll_call_as_main(void *dll, const char *name);

#endif

PROJECTS := entities keyword f16 closure
.PHONY : all clean cxxcheck $(PROJECTS)
.DEFAULT_GOAL := all

CSOURCES := closure.c
CXXSOURCES := entities.c keyword.c f16.c
SOURCES :=  $(CXXSOURCES) $(CSOURCES)
OBJECTS := $(SOURCES:%.c=%.o)
TESTS := $(SOURCES:%.c=t-%)
GARBAGE := $(OBJECTS) $(TESTS)

CLANG := clang -std=c99 -Werror -Weverything
CLANGXX := clang++ -std=c++98 -Werror -Weverything -xc++
GCC := gcc -std=c99 -pedantic -Werror -Wall -Wextra
CFLAGS := -O3 -ggdb3

CHECK_SYNTAX = $(CLANG) -fsyntax-only $(NOWARN:%=-Wno-%) $<
COMPILE = $(GCC) -c $(CFLAGS) -o $@ $<
BUILD = $(GCC) $(CFLAGS) -o $@ $^
CLEAN = rm -f $(GARBAGE)
CXXCHECK = $(CLANGXX) -fsyntax-only $(NOWARN:%=-Wno-%) $(CXXSOURCES)
RUN = @set -e; for BIN in $^; do echo ./$$BIN; ./$$BIN; done

all : $(TESTS)
	$(RUN)

clean :
	$(CLEAN)

cxxcheck : NOWARN += c++11-long-long
cxxcheck :
	$(CXXCHECK)

$(PROJECTS) : % : t-%
	./$<

t-f16 : NOWARN += float-equal
$(TESTS) : t-% : t-%.c %.o
	$(CHECK_SYNTAX)
	$(BUILD)

$(OBJECTS) : %.o : %.c %.h
	$(CHECK_SYNTAX)
	$(COMPILE)
